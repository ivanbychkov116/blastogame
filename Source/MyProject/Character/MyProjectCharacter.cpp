// Copyright Epic Games, Inc. All Rights Reserved.

#include "MyProjectCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "MyProject/Game/MyProjectGameInstance.h"

//////////////////////////////////////////////////////////////////////////
// AMyProjectCharacter

AMyProjectCharacter::AMyProjectCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = true;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = true; // Camera does not rotate relative to arm

	ArrowL = CreateDefaultSubobject<UArrowComponent>(TEXT("ArrowL"));
	ArrowR = CreateDefaultSubobject<UArrowComponent>(TEXT("ArrowR"));

	ArrowL->SetupAttachment(RootComponent);
	ArrowR->SetupAttachment(RootComponent);

	InventoryComponent = CreateDefaultSubobject<UBlastoInventoryComponent>(TEXT("InventoryComponent"));

	if(InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &AMyProjectCharacter::InitWeapon);
	}

	// 
	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}

//////////////////////////////////////////////////////////////////////////
// Input

void AMyProjectCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &AMyProjectCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AMyProjectCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AMyProjectCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AMyProjectCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &AMyProjectCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &AMyProjectCharacter::TouchStopped);

	// Left mouse button attack
	PlayerInputComponent->BindAction(TEXT("FireLMB"), EInputEvent::IE_Pressed, this, &AMyProjectCharacter::LMB_InputAttackPressed);
	PlayerInputComponent->BindAction(TEXT("FireLMB"), EInputEvent::IE_Released, this, &AMyProjectCharacter::LMB_InputAttackReleased);

	// Right mouse button attack
	PlayerInputComponent->BindAction(TEXT("FireRMB"), EInputEvent::IE_Pressed, this, &AMyProjectCharacter::RMB_InputAttackPressed);
	PlayerInputComponent->BindAction(TEXT("FireRMB"), EInputEvent::IE_Released, this, &AMyProjectCharacter::RMB_InputAttackReleased);

}

void AMyProjectCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
		Jump();
}

void AMyProjectCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
		StopJumping();
}

void AMyProjectCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AMyProjectCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AMyProjectCharacter::MoveForward(float Value)
{
	if ((Controller != nullptr) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AMyProjectCharacter::MoveRight(float Value)
{
	if ( (Controller != nullptr) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void AMyProjectCharacter::BeginPlay()
{
	Super::BeginPlay();

}

FProjectileInfo AMyProjectCharacter::GetSpawnProjectile(int32 currentProjectile)
{
	switch (currentProjectile)
	{
	case 0:
		return RedProjectile;
	case 1:
		return YellowProjectile;
	case 2:
		return BlueProjectile;
	case 3:
		return WhiteProjectile;
	default:
		return RedProjectile;
	}
}

FVector AMyProjectCharacter::GetLaserSphereColor(int32 currentColor)
{
	switch (currentColor)
	{
	case 0:
		return RedSphereColor;
	case 1:
		return YellowSphereColor;
	case 2:
		return BlueSphereColor;
	case 3:
		return WhiteSphereColor;
	default:
		return RedSphereColor;
	}
}

void AMyProjectCharacter::LMB_InputAttackPressed()
{
	if (!IsCharHanging && !IsClimbLedge && !RightAttack && CanRotateChar)
	{
		LMB_AttackCharEvent(true);
		LeftAttack = true;
	}
}

void AMyProjectCharacter::LMB_InputAttackReleased()
{
	LMB_AttackCharEvent(false);
	LeftAttack = false;
}

void AMyProjectCharacter::RMB_InputAttackPressed()
{
	if (!IsCharHanging && !IsClimbLedge && !LeftAttack && CanRotateChar)
	{
		RMB_AttackCharEvent(true);
		RightAttack = true;
	}

}


void AMyProjectCharacter::RMB_InputAttackReleased()
{
	RMB_AttackCharEvent(false);
	RightAttack = false;
}

void AMyProjectCharacter::LMB_AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		//ToDo Check melee or range
		myWeapon->LMB_SetWeaponStateFire(bIsFiring);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("AMyProjectCharacter::AttackCharEvent - CurrentWeapon -NULL"));
}

void AMyProjectCharacter::RMB_AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		//ToDo Check melee or range
		myWeapon->RMB_SetWeaponStateFire(bIsFiring);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("AMyProjectCharacter::AttackCharEvent - CurrentWeapon -NULL"));
}

AWeaponDefault* AMyProjectCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void AMyProjectCharacter::InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo)
{
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}

	UMyProjectGameInstance* myGI = Cast<UMyProjectGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGI)
	{
		if (myGI->GetWeaponInfoByName(IdWeaponName, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("GunSocket"));
					CurrentWeapon = myWeapon;
					myWeapon->CurrentWeaponIdName = IdWeaponName;

					myWeapon->GetCameraPtr(FollowCamera);
					myWeapon->WeaponSetting = myWeaponInfo;


					// test!!!!
					myWeapon->WeaponSetting.FirstProjectileSetting = GetSpawnProjectile(CurrentProjectileType);
					//----------


					myWeapon->WeaponInfo.Round = myWeaponInfo.MaxRound;
					myWeapon->WeaponInfo = WeaponAdditionalInfo;

					if (InventoryComponent)
					{
						CurrentIndexWeapon = InventoryComponent->GetWeaponIndexSlotByName(IdWeaponName);
						if(CurrentIndexWeapon != 0)
							InventoryComponent->SetAdditionalWeaponInfo(CurrentIndexWeapon, CurrentWeapon->WeaponInfo);
					}

					myWeapon->OnWeaponFireStart.AddDynamic(this, &AMyProjectCharacter::WeaponFireStart);
				}
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("AMyProjectCharacter::InitWeapon - Weapon not found in table -NULL"));
		}
	}
}


void AMyProjectCharacter::WeaponFireStart(UAnimMontage* Anim)
{
	if(RightAttack)
		InventoryComponent->SetAdditionalWeaponInfo(CurrentIndexWeapon, CurrentWeapon->WeaponInfo);

	WeaponFireStart_BP(Anim);
}


void AMyProjectCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* Anim)
{
	// in BP
}

void AMyProjectCharacter::TrySwicthNextWeapon()
{
	CurrentWeapon->WeaponInfo.Round = CurrentWeapon->WeaponSetting.MaxRound;
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->WeaponInfo;
		}

		if (InventoryComponent)
		{
			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon + 1, OldIndex, OldInfo))
			{
			}
		}
	}
}

void AMyProjectCharacter::TrySwitchPreviosWeapon()
{
	CurrentWeapon->WeaponInfo.Round = CurrentWeapon->WeaponSetting.MaxRound;
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->WeaponInfo;
		}

		if (InventoryComponent)
		{
			//InventoryComponent->SetAdditionalInfoWeapon(OldIndex, GetCurrentWeapon()->AdditionalWeaponInfo);
			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon - 1, OldIndex, OldInfo))
			{
			}
		}
	}
}