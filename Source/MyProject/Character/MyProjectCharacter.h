// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Components/ArrowComponent.h"
#include "MyProject/FunctionLibrary/Types.h"
#include "MyProject/Weapon/WeaponDefault.h"
#include "MyProject/Weapon/ProjectileDefault.h"
#include "MyProject/BlastoInventoryComponent.h"

#include "MyProjectCharacter.generated.h"


UCLASS(config=Game)
class AMyProjectCharacter : public ACharacter
{
	GENERATED_BODY()


	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

protected:
	virtual void BeginPlay() override;

public:
	AMyProjectCharacter();


	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UBlastoInventoryComponent* InventoryComponent;


	// weapon attack system
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "ProjectileSetting")
		FProjectileInfo RedProjectile;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "ProjectileSetting")
		FProjectileInfo YellowProjectile;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "ProjectileSetting")
		FProjectileInfo BlueProjectile;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "ProjectileSetting")
		FProjectileInfo WhiteProjectile;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		int32 CurrentProjectileType = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AttackSystem")
		bool LeftAttack = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AttackSystem")
		bool RightAttack = false;

	// Weapon's laser sphere colors
	UPROPERTY(BlueprintReadOnly, Category = "SphereColors")
		FVector RedSphereColor = FVector(1.0f, 0.068f, 0.042f);
	UPROPERTY(BlueprintReadOnly, Category = "SphereColors")
		FVector YellowSphereColor = FVector(0.85f, 0.75f, 0.0f);
	UPROPERTY(BlueprintReadOnly, Category = "SphereColors")
		FVector BlueSphereColor = FVector(0.044f, 0.33f, 0.85f);
	UPROPERTY(BlueprintReadOnly, Category = "SphereColors")
		FVector WhiteSphereColor = FVector(1.0f, 1.0f, 1.0f);

	//Weapon	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Weapon")
		AWeaponDefault* CurrentWeapon = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anims")
		bool IsCharHanging = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anims")
		bool IsClimbLedge = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anims")
		bool CanRotateChar = true;

	// climb movement system
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UArrowComponent* ArrowL = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UArrowComponent* ArrowR = nullptr;

protected:

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	//Func
	UFUNCTION(BlueprintCallable)
		void LMB_AttackCharEvent(bool bIsFiring);
	UFUNCTION(BlueprintCallable)
		void RMB_AttackCharEvent(bool bIsFiring);

	UFUNCTION(BlueprintCallable)
		AWeaponDefault* GetCurrentWeapon();

	UFUNCTION(BlueprintCallable)
		void InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo);

	UFUNCTION()
		void WeaponFireStart(UAnimMontage* Anim);

	UFUNCTION(BlueprintNativeEvent)
		void WeaponFireStart_BP(UAnimMontage* Anim);

	UFUNCTION(BlueprintCallable)
		FProjectileInfo GetSpawnProjectile(int32 currentProjectile);

	UFUNCTION(BlueprintCallable)
		FVector GetLaserSphereColor(int32 currentColor);

	//Inputs
	UFUNCTION(BlueprintCallable)
		void LMB_InputAttackPressed();
	UFUNCTION(BlueprintCallable)
		void LMB_InputAttackReleased();
	UFUNCTION(BlueprintCallable)
		void RMB_InputAttackPressed();
	UFUNCTION(BlueprintCallable)
		void RMB_InputAttackReleased();

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
		int32 CurrentIndexWeapon = 0;

	UFUNCTION(BlueprintCallable)
		void TrySwicthNextWeapon();
	UFUNCTION(BlueprintCallable)
		void TrySwitchPreviosWeapon();
};

