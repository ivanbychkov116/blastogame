// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"

#include "MyProject/FunctionLibrary/Types.h"
#include "MyProject/Weapon/ProjectileDefault.h"
#include "WeaponDefault.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponFireStart, UAnimMontage*, Anim);

UCLASS()
class MYPROJECT_API AWeaponDefault : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AWeaponDefault();

	FOnWeaponFireStart OnWeaponFireStart;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float FectorLenght = 0.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UStaticMeshComponent* StaticMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UArrowComponent* ShootLocation = nullptr;

	class UCameraComponent* FollowCamera = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Weapon Info")
		FWeaponInfo WeaponSetting;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
		FAdditionalWeaponInfo WeaponInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
		bool LMB_WeaponFiring = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
		bool RMB_WeaponFiring = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
		bool DoubleClickRMB = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
		bool CharHanging = false;

	bool IsCharging = false;
	FName CurrentWeaponIdName;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Tick func
	virtual void Tick(float DeltaTime) override;

	void FireTick(float DeltaTime);

	void WeaponInit();

	UFUNCTION(BlueprintCallable)
		void LMB_SetWeaponStateFire(bool bIsFire);
	UFUNCTION(BlueprintCallable)
		void RMB_SetWeaponStateFire(bool bIsFire);

	bool LMB_CheckWeaponCanFire();
	bool RMB_CheckWeaponCanFire();

	FProjectileInfo GetFirstProjectile();
	FProjectileInfo GetSecondProjectile();
	FProjectileInfo GetThirdProjectile();

	void LMB_Fire();
	void RMB_Fire();
	//FVector ApplyDispersionToShoot(FVector DirectionShoot)const;

	//Timers
	float FireTimer = 0.0f;

	//flags
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
		bool LMB_BlockFire = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
		bool RMB_BlockFire = false;

	UFUNCTION(BlueprintCallable)
		int32 GetWeaponRound();

	UFUNCTION(BlueprintCallable)
		void SpawnSuperProjectile(FProjectileInfo currentProjectile);

	UFUNCTION(BlueprintCallable)
		void SpawnProjectile(FProjectileInfo CurrentProjectile);

	void GetCameraPtr(UCameraComponent* CharCamera);

};
