// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponDefault.h"
#include "Camera/CameraComponent.h"
#include "DrawDebugHelpers.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "MyProject/BlastoInventoryComponent.h"

// Sets default values
AWeaponDefault::AWeaponDefault()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh "));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);
	ShootLocation->SetVisibility(true);

}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();

	WeaponInit();

}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FireTick(DeltaTime);

}

void AWeaponDefault::FireTick(float DeltaTime)
{
	if (LMB_WeaponFiring && !RMB_WeaponFiring)
	{
		if (FireTimer < 0.f)
		{
			LMB_Fire();
		}
		else
			FireTimer -= DeltaTime;
	}
	if (GetWeaponRound() > 0)
	{
		if (RMB_WeaponFiring && !LMB_WeaponFiring)
			if (FireTimer < 0.f)
			{
					RMB_Fire();
			}
			else
				FireTimer -= DeltaTime;
	}
	else
	{
		// delete super gun and give standart
	}
}


void AWeaponDefault::WeaponInit()
{
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
	{
		SkeletalMeshWeapon->DestroyComponent(true);
	}

	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent();
	}

}

void AWeaponDefault::LMB_SetWeaponStateFire(bool bIsFire)
{
	if (LMB_CheckWeaponCanFire())
		LMB_WeaponFiring = bIsFire;
	else
		LMB_WeaponFiring = false;

	if (bIsFire)
	{
		RMB_BlockFire = true;
	}
	else
	{
		if (RMB_BlockFire)
		{
			LMB_BlockFire = false;
		}
		RMB_BlockFire = false;
		IsCharging = false;
	}
	if (!RMB_WeaponFiring)
		FireTimer = 0.01f;
}

void AWeaponDefault::RMB_SetWeaponStateFire(bool bIsFire)
{
	if (RMB_CheckWeaponCanFire())
		RMB_WeaponFiring = bIsFire;
	else
		RMB_WeaponFiring = false;

	if (bIsFire)
	{
		LMB_BlockFire = true;
	}
	else
	{
		if (!IsCharging)
		{
			LMB_BlockFire = false;
		}
	}
	if(!LMB_WeaponFiring)
		FireTimer = 0.01f;
}

bool AWeaponDefault::LMB_CheckWeaponCanFire()
{
	return !LMB_BlockFire;
}

bool AWeaponDefault::RMB_CheckWeaponCanFire()
{
	return !RMB_BlockFire;
}

FProjectileInfo AWeaponDefault::GetFirstProjectile()
{
	return WeaponSetting.FirstProjectileSetting;
}

FProjectileInfo AWeaponDefault::GetSecondProjectile()
{
	return WeaponSetting.SecondProjectileSetting;
}

FProjectileInfo AWeaponDefault::GetThirdProjectile()
{
	return WeaponSetting.ThirdProjectileSetting;
}

void AWeaponDefault::LMB_Fire()
{
	FireTimer = WeaponSetting.RateOfFire;
	if (!LMB_BlockFire)
	{
		SpawnProjectile(GetFirstProjectile());
		LMB_BlockFire = true;
		IsCharging = true;
	}
}
void AWeaponDefault::RMB_Fire()
{
	FireTimer = WeaponSetting.RateOfFire;
	if (!CharHanging)
	{
		if (!DoubleClickRMB)
		{
			WeaponInfo.Round -= 1; // remove 1 ammo from clips
			switch (WeaponSetting.WeaponType)
			{
			case EWeaponType::StandartBlasterType:
				UGameplayStatics::SpawnSoundAtLocation(GetWorld(), GetSecondProjectile().ProjectileFireSound, ShootLocation->GetComponentLocation());
				RMB_WeaponFiring = false;
				break;
			case EWeaponType::RapidFireBlaserType:
				SpawnProjectile(GetFirstProjectile());
				break;
			case EWeaponType::RocketBlasterType:
				SpawnProjectile(GetSecondProjectile());
				break;
			case EWeaponType::GrenadeBlasterType:
				SpawnProjectile(GetSecondProjectile());
				break;
			default:
				UGameplayStatics::SpawnSoundAtLocation(GetWorld(), GetSecondProjectile().ProjectileFireSound, ShootLocation->GetComponentLocation());
				++WeaponInfo.Round;
				RMB_WeaponFiring = false;
			}
		}
		else
		{
			WeaponInfo.Round -= 20; // super projectile cost 20 ammo
			switch (WeaponSetting.WeaponType)
			{
			case EWeaponType::StandartBlasterType:
				UGameplayStatics::SpawnSoundAtLocation(GetWorld(), GetSecondProjectile().ProjectileFireSound, ShootLocation->GetComponentLocation());
				++WeaponInfo.Round;
				RMB_WeaponFiring = false;
				break;
			case EWeaponType::RapidFireBlaserType:
				SpawnProjectile(GetThirdProjectile());
				break;
			case EWeaponType::RocketBlasterType:
				SpawnProjectile(GetThirdProjectile());
				break;
			case EWeaponType::GrenadeBlasterType:
				SpawnProjectile(GetThirdProjectile());
				break;
			default:
				UGameplayStatics::SpawnSoundAtLocation(GetWorld(), GetSecondProjectile().ProjectileFireSound, ShootLocation->GetComponentLocation());
				++WeaponInfo.Round;
				RMB_WeaponFiring = false;
			}
		}
		/*if (GetOwner())
		{
			UBlastoInventoryComponent* MyInv = Cast<UBlastoInventoryComponent>(GetOwner()->GetComponentByClass(UBlastoInventoryComponent::StaticClass()));
			if (MyInv)
			{
				MyInv->SetAdditionalWeaponInfo(MyInv->GetWeaponIndexSlotByName(CurrentWeaponIdName), WeaponInfo);
			}
		}*/
	}
}


//FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionShoot) const
//{
//	return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.f);
//}


int32 AWeaponDefault::GetWeaponRound()
{
	return WeaponInfo.Round;
}

void AWeaponDefault::SpawnSuperProjectile(FProjectileInfo currentProjectile)
{
	SpawnProjectile(currentProjectile);
	IsCharging = false;	
}

void AWeaponDefault::GetCameraPtr(UCameraComponent* CharCamera)
{
	FollowCamera = CharCamera;
}

void AWeaponDefault::SpawnProjectile(FProjectileInfo CurrentProjectile)
{
	if (ShootLocation)
	{
		FRotator SpawnRotation = ShootLocation->GetComponentRotation();
		FProjectileInfo ProjectileInfo;
		ProjectileInfo = CurrentProjectile;

		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), ProjectileInfo.ProjectileFireSound, ShootLocation->GetComponentLocation());
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSetting.EffectFireWeapon, ShootLocation->GetComponentTransform());

		FVector EndLocation;
		FVector SpawnLocation = ShootLocation->GetComponentLocation();

		if (FollowCamera)
		{
			FVector EndOfTrace = FollowCamera->GetForwardVector() * 1000000.0f + FollowCamera->GetComponentLocation();
			FVector StartOfTrace = FollowCamera->GetForwardVector() * 300.0f + FollowCamera->GetComponentLocation();
			FCollisionQueryParams TraceParams;
			FHitResult Hit;

			bool IsCrossed = GetWorld()->LineTraceSingleByChannel(Hit, StartOfTrace, EndOfTrace, ECC_Visibility, TraceParams);

			if (IsCrossed)
			{
				// for debug trace cast
				DrawDebugLine(GetWorld(), StartOfTrace, EndOfTrace, FColor::Red, false, 10.0f);
				DrawDebugBox(GetWorld(), Hit.Location, FVector(10, 10, 10), FColor::Green, false, 2.0f);
				EndLocation = Hit.Location;
				FectorLenght = sqrt(pow((Hit.Location.X - StartOfTrace.X), 2) + pow((Hit.Location.Y - StartOfTrace.Y), 2) + pow((Hit.Location.Z - StartOfTrace.Z), 2));
				if (FectorLenght < 200.0f)
				{
					EndLocation = FollowCamera->GetForwardVector() * 2000.0f + FollowCamera->GetComponentLocation();
				}
			}
			else
			{
				EndLocation = FollowCamera->GetForwardVector() * 2000.0f + FollowCamera->GetComponentLocation();
			}
		}

		FVector Dir = EndLocation - SpawnLocation;

		Dir.Normalize();

		FMatrix myMatrix(Dir, FVector(0, 0, 0), FVector(0, 0, 0), FVector::ZeroVector);
		SpawnRotation = myMatrix.Rotator();


		if (ProjectileInfo.Projectile)
		{
			//Projectile Init ballistic fire

			FActorSpawnParameters SpawnParams;
			SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			SpawnParams.Owner = GetOwner();
			SpawnParams.Instigator = GetInstigator();



			AProjectileDefault* myProjectile = Cast<AProjectileDefault>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));
			if (myProjectile)
			{
				myProjectile->InitProjectile(CurrentProjectile);
				if (WeaponSetting.AnimCharFire)
					OnWeaponFireStart.Broadcast(WeaponSetting.AnimCharFire);
			}

		}
		else
		{
			//ToDo Projectile null Init trace fire			
			//GetWorld()->LineTraceSingleByChannel()
		}
	}
}
