// Fill out your copyright notice in the Description page of Project Settings.


#include "BlastoInventoryComponent.h"
#include "MyProject/Game/MyProjectGameInstance.h"
#pragma optimize ("", off)

// Sets default values for this component's properties
UBlastoInventoryComponent::UBlastoInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UBlastoInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...

	// Find init WeaponSlots and First Init Weapon
	for (int8 i = 0; i < WeaponSlots.Num(); ++i)
	{
		UMyProjectGameInstance* myGI = Cast<UMyProjectGameInstance>(GetWorld()->GetGameInstance());
		if (myGI)
		{
			if (!WeaponSlots[i].NameItem.IsNone())
			{
				FWeaponInfo WeaponInfo;
				if (myGI->GetWeaponInfoByName(WeaponSlots[i].NameItem, WeaponInfo))
				{
					WeaponSlots[i].AdditionalInfo.Round = WeaponInfo.MaxRound;
				}
				else
				{
					WeaponSlots.RemoveAt(i);
					--i;
				}
			}
		}
	}
	
	MaxSlotsWeapon = WeaponSlots.Num();
	if (WeaponSlots.IsValidIndex(0))
	{
		if (!WeaponSlots[0].NameItem.IsNone())
		{
			OnSwitchWeapon.Broadcast(WeaponSlots[0].NameItem, WeaponSlots[0].AdditionalInfo);
		}
	}
}


// Called every frame
void UBlastoInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

bool UBlastoInventoryComponent::SwitchWeaponToIndex(int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo)
{
	bool bIsSuccess = false;
	int8 CorrectIndex = ChangeToIndex;
	if (ChangeToIndex > WeaponSlots.Num() - 1)
		CorrectIndex = 0;
	else
		if (ChangeToIndex < 0)
			CorrectIndex = WeaponSlots.Num() - 1;

	FName NewIdWeapon;
	FAdditionalWeaponInfo NewAdditionalInfo;

	int8 i = 0;
	while (i < WeaponSlots.Num() && !bIsSuccess)
	{
		// set new weapon info
		if (i == CorrectIndex)
		{
			if (!WeaponSlots[i].NameItem.IsNone())
			{
				NewIdWeapon = WeaponSlots[i].NameItem;
				NewAdditionalInfo = WeaponSlots[i].AdditionalInfo;
				bIsSuccess = true;
			}
		}
		++i;
	}

	if (!bIsSuccess)
	{

	}

	if (bIsSuccess)
	{
		if(CorrectIndex == 0)
			IsFromChangeWeapon = true;
		SetAdditionalWeaponInfo(OldIndex, OldInfo);
		OnSwitchWeapon.Broadcast(NewIdWeapon, NewAdditionalInfo);
		IsFromChangeWeapon = false;
	}

	return bIsSuccess;
}

FAdditionalWeaponInfo UBlastoInventoryComponent::GetAdditionalWeaponInfo(int32 IndexWeapon)
{
	FAdditionalWeaponInfo result;
	if (WeaponSlots.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int8 i = 0;
		while (i < WeaponSlots.Num() && !bIsFind)
		{
			if (i == IndexWeapon)
			{
				result = WeaponSlots[i].AdditionalInfo;
				bIsFind = true;
			}
			++i;
		}
		if(!bIsFind)
			UE_LOG(LogTemp, Warning, TEXT("UBlastoInventoryComponent::SetAdditionalInfoWeapon - No Found Weapon with index - %d"), IndexWeapon);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("UBlastoInventoryComponent::SetAdditionalInfoWeapon - Not Correct index Weapon - %d"), IndexWeapon);
	return result;
}

int32 UBlastoInventoryComponent::GetWeaponIndexSlotByName(FName IdWeaponName)
{
	int32 result = -1;
	int8 i = 0;
	bool bIsFind = false;
	while (i < WeaponSlots.Num() && !bIsFind)
	{
		if (WeaponSlots[i].NameItem == IdWeaponName)
		{
			result = i;
			bIsFind = true;
		}
		++i;
	}
	return result;
}

void UBlastoInventoryComponent::SetAdditionalWeaponInfo(int32 IndexWeapon, FAdditionalWeaponInfo NewInfo)
{
	if (WeaponSlots.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int i = 0;
		while (i < WeaponSlots.Num() && !bIsFind)
		{
			if (i == IndexWeapon)
			{
				WeaponSlots[i].AdditionalInfo = NewInfo;
				bIsFind = true;
				if(!IsFromChangeWeapon)
					OnWeaponAdditionalInfoChange.Broadcast(WeaponSlots[i].AdditionalInfo.Round);
			}
			++i;
		}
		if (!bIsFind)
		{
			UE_LOG(LogTemp, Warning, TEXT("UBlastoInventoryComponent::SetAdditionalInfoWeapon - No Found Weapon with index - %d"), IndexWeapon);
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UBlastoInventoryComponent::SetAdditionalInfoWeapon - Not Correct index Weapon - %d"), IndexWeapon);
	}
}
